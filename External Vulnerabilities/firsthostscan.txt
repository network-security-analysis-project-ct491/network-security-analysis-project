# Nmap 7.93 scan initiated Thu Feb 23 19:12:08 2023 as: nmap -oN /home/kali/firsthostscan.txt -sP 144.118.0.0/16
Nmap scan report for gpvpn.drexel.edu (144.118.1.161)
Host is up (0.0067s latency).
Nmap scan report for gp-irt-private.noc.drexel.edu (144.118.1.163)
Host is up (0.0057s latency).
Nmap scan report for gp-drexel-trusted-clients.noc.drexel.edu (144.118.1.164)
Host is up (0.029s latency).
Nmap scan report for gp-irt-application-test.noc.drexel.edu (144.118.1.165)
Host is up (0.026s latency).
Nmap scan report for purplestar.irt.drexel.edu (144.118.6.62)
Host is up (0.0068s latency).
Nmap scan report for viavpn1.noc.drexel.edu (144.118.20.240)
Host is up (0.034s latency).
Nmap scan report for viavpn2.noc.drexel.edu (144.118.20.241)
Host is up (0.0082s latency).
Nmap scan report for n2-22-109.dhcp.drexel.edu (144.118.22.109)
Host is up (0.045s latency).
Nmap scan report for myvirtual.drexel.edu (144.118.23.129)
Host is up (0.027s latency).
Nmap scan report for cti-xtwweb001.irttest.drexel.edu (144.118.32.18)
Host is up (0.023s latency).
Nmap scan report for cti-xpwweb001.irt.drexel.edu (144.118.32.19)
Host is up (0.022s latency).
Nmap scan report for cti-xpwscc019.irt.drexel.edu (144.118.32.72)
Host is up (0.051s latency).
Nmap scan report for cti-xpwscc020.irt.drexel.edu (144.118.32.73)
Host is up (0.051s latency).
Nmap scan report for cti-xtwscc007.irttest.drexel.edu (144.118.32.77)
Host is up (0.072s latency).
Nmap scan report for cti-xtwscc008.irttest.drexel.edu (144.118.32.78)
Host is up (0.048s latency).
Nmap scan report for jay.irt.drexel.edu (144.118.38.12)
Host is up (0.0045s latency).
Nmap scan report for rmcp.dcollege.net (144.118.38.51)
Host is up (0.0060s latency).
Nmap scan report for highwater.irt.drexel.edu (144.118.38.116)
Host is up (0.011s latency).
Nmap scan report for cti-kpwmed001.medaille.drexel.edu (144.118.38.131)
Host is up (0.014s latency).
Nmap scan report for pmt.irttest.dcollege.net (144.118.38.142)
Host is up (0.0077s latency).
Nmap scan report for orgplus.drexel.edu (144.118.38.153)
Host is up (0.0063s latency).
Nmap scan report for anchor.irt.drexel.edu (144.118.38.184)
Host is up (0.014s latency).
Nmap scan report for dornsife.drexel.edu (144.118.39.16)
Host is up (0.0060s latency).
Nmap scan report for barreleye.irttest.drexel.edu (144.118.39.17)
Host is up (0.0062s latency).
Nmap scan report for batray.irt.drexel.edu (144.118.39.18)
Host is up (0.0072s latency).
Nmap scan report for drexelmed.edu (144.118.39.83)
Host is up (0.020s latency).
Nmap scan report for maltese.irt.drexel.edu (144.118.39.126)
Host is up (0.010s latency).
Nmap scan report for lf.irt.drexel.edu (144.118.39.132)
Host is up (0.011s latency).
Nmap scan report for batcreek.irt.drexel.edu (144.118.39.135)
Host is up (0.011s latency).
Nmap scan report for akero.irt.drexel.edu (144.118.39.179)
Host is up (0.0044s latency).
Nmap scan report for www.baiadacenter.drexel.edu (144.118.39.215)
Host is up (0.0060s latency).
Nmap scan report for www.biomed.mcphu.edu (144.118.39.216)
Host is up (0.059s latency).
Nmap scan report for ltgmedia.online.drexel.edu (144.118.45.192)
Host is up (0.013s latency).
Nmap scan report for n2-56-102.dhcp.drexel.edu (144.118.56.102)
Host is up (0.0096s latency).
Nmap scan report for beta-local-v500.noc.drexel.edu (144.118.63.226)
Host is up (0.013s latency).
Nmap scan report for banner.irttest.drexel.edu (144.118.66.101)
Host is up (0.011s latency).
Nmap scan report for banner-dev.irttest.drexel.edu (144.118.66.102)
Host is up (0.010s latency).
Nmap scan report for bannerforms-dev.irttest.drexel.edu (144.118.66.103)
Host is up (0.027s latency).
Nmap scan report for webedit.irttest.drexel.edu (144.118.66.112)
Host is up (0.010s latency).
Nmap scan report for webedit.drexel.edu (144.118.66.113)
Host is up (0.017s latency).
Nmap scan report for www-future.drexel.edu (144.118.66.114)
Host is up (0.015s latency).
Nmap scan report for eprocure.irttest.drexel.edu (144.118.66.198)
Host is up (0.0084s latency).
Nmap scan report for eprocure.drexel.edu (144.118.66.199)
Host is up (0.0071s latency).
Nmap scan report for proxiedapps-prvt.irttest.drexel.edu (144.118.66.216)
Host is up (0.028s latency).
Nmap scan report for proxiedapps-prvt.irt.drexel.edu (144.118.66.217)
Host is up (0.022s latency).
Nmap scan report for gradcollege.irttest.drexel.edu (144.118.67.1)
Host is up (0.0072s latency).
Nmap scan report for gradcollege.irt.drexel.edu (144.118.67.2)
Host is up (0.015s latency).
Nmap scan report for webapps.irttest.drexel.edu (144.118.67.3)
Host is up (0.0097s latency).
Nmap scan report for webapps.irt.drexel.edu (144.118.67.4)
Host is up (0.0068s latency).
Nmap scan report for shib-irttest.medaille.drexel.edu (144.118.67.5)
Host is up (0.0065s latency).
Nmap scan report for redcaptraining.irttest.drexel.edu (144.118.67.6)
Host is up (0.012s latency).
Nmap scan report for redcap.drexel.edu (144.118.67.7)
Host is up (0.016s latency).
Nmap scan report for shib.irttest.drexel.edu (144.118.67.8)
Host is up (0.020s latency).
Nmap scan report for carbon.irttest.drexel.edu (144.118.67.9)
Host is up (0.018s latency).
Nmap scan report for drexel.edu (144.118.67.10)
Host is up (0.011s latency).
Nmap scan report for ansp.org (144.118.67.11)
Host is up (0.024s latency).
Nmap scan report for library.drexel.edu (144.118.67.12)
Host is up (0.021s latency).
Nmap scan report for aacorn.org (144.118.67.13)
Host is up (0.014s latency).
Nmap scan report for philadelphiaamp.com (144.118.67.14)
Host is up (0.012s latency).
Nmap scan report for dev.drexelmedicine.org (144.118.67.15)
Host is up (0.0098s latency).
Nmap scan report for webpub.drexel.edu (144.118.67.16)
Host is up (0.0094s latency).
Nmap scan report for www-user.irttest.drexel.edu (144.118.67.17)
Host is up (0.013s latency).
Nmap scan report for payment-drexelmed.irt.drexel.edu (144.118.67.18)
Host is up (0.035s latency).
Nmap scan report for one.drexel.edu (144.118.67.19)
Host is up (0.021s latency).
Nmap scan report for bannersso-irttest.medaille.drexel.edu (144.118.67.21)
Host is up (0.019s latency).
Nmap scan report for connect.medaille.drexel.edu (144.118.67.22)
Host is up (0.0084s latency).
Nmap scan report for dragoncardmanager.drexel.edu (144.118.67.23)
Host is up (0.018s latency).
Nmap scan report for test.ansp.org (144.118.67.24)
Host is up (0.013s latency).
Nmap scan report for irttest.drexel.edu (144.118.67.25)
Host is up (0.017s latency).
Nmap scan report for employer.steinbright.drexel.edu (144.118.67.27)
Host is up (0.0097s latency).
Nmap scan report for banner.drexel.edu (144.118.67.28)
Host is up (0.013s latency).
Nmap scan report for banner.medaille.drexel.edu (144.118.67.29)
Host is up (0.012s latency).
Nmap scan report for lacurbanhealth.org (144.118.67.30)
Host is up (0.013s latency).
Nmap scan report for 144.118.67.31
Host is up (0.010s latency).
Nmap scan report for connect.drexel.edu (144.118.67.32)
Host is up (0.012s latency).
Nmap scan report for networkregistration.drexel.edu (144.118.67.33)
Host is up (0.0090s latency).
Nmap scan report for docss.med.drexel.edu (144.118.67.34)
Host is up (0.0100s latency).
Nmap scan report for tickets.irt.drexel.edu (144.118.67.35)
Host is up (0.019s latency).
Nmap scan report for bannersso.drexel.edu (144.118.67.36)
Host is up (0.014s latency).
Nmap scan report for bannersso.medaille.drexel.edu (144.118.67.37)
Host is up (0.020s latency).
Nmap scan report for webcampus.med.drexel.edu (144.118.67.38)
Host is up (0.0061s latency).
Nmap scan report for apps.med.drexel.edu (144.118.67.39)
Host is up (0.011s latency).
Nmap scan report for proxiedapps.irt.drexel.edu (144.118.67.40)
Host is up (0.020s latency).
Nmap scan report for irtstudentservices.irttest.drexel.edu (144.118.67.41)
Host is up (0.014s latency).
Nmap scan report for proxiedapps.medaille.drexel.edu (144.118.67.42)
Host is up (0.0079s latency).
Nmap scan report for apply.med.drexel.edu (144.118.67.43)
Host is up (0.013s latency).
Nmap scan report for webapi.irttest.drexel.edu (144.118.67.44)
Host is up (0.040s latency).
Nmap scan report for webapi.irt.drexel.edu (144.118.67.45)
Host is up (0.010s latency).
Nmap scan report for carbon.irt.drexel.edu (144.118.67.46)
Host is up (0.016s latency).
Nmap scan report for viz.drexel.edu (144.118.67.47)
Host is up (0.011s latency).
Nmap scan report for esm.irttest.drexel.edu (144.118.67.48)
Host is up (0.010s latency).
Nmap scan report for esm.irt.drexel.edu (144.118.67.49)
Host is up (0.013s latency).
Nmap scan report for esm-jenkins.irttest.drexel.edu (144.118.67.50)
Host is up (0.0094s latency).
Nmap scan report for esm-jenkins.irt.drexel.edu (144.118.67.51)
Host is up (0.015s latency).
Nmap scan report for grafana.irttest.drexel.edu (144.118.67.53)
Host is up (0.074s latency).
Nmap scan report for events.irttest.drexel.edu (144.118.67.54)
Host is up (0.015s latency).
Nmap scan report for grafana.irt.drexel.edu (144.118.67.55)
Host is up (0.016s latency).
Nmap scan report for ems.irttest.drexel.edu (144.118.67.56)
Host is up (0.0075s latency).
Nmap scan report for ems.drexel.edu (144.118.67.57)
Host is up (0.0095s latency).
Nmap scan report for a4l-t.irttest.drexel.edu (144.118.67.58)
Host is up (0.011s latency).
Nmap scan report for a4l.irt.drexel.edu (144.118.67.59)
Host is up (0.0066s latency).
Nmap scan report for reporting.drexel.edu (144.118.67.60)
Host is up (0.011s latency).
Nmap scan report for accounts.drexel.edu (144.118.67.61)
Host is up (0.013s latency).
Nmap scan report for reports-dev.irttest.dcollege.net (144.118.67.62)
Host is up (0.0078s latency).
Nmap scan report for reports.irttest.dcollege.net (144.118.67.63)
Host is up (0.0070s latency).
Nmap scan report for bannerweb.irttest.drexel.edu (144.118.67.67)
Host is up (0.013s latency).
Nmap scan report for ccbirs.psd.drexel.edu (144.118.67.68)
Host is up (0.019s latency).
Nmap scan report for dwapps.drexel.edu (144.118.67.69)
Host is up (0.013s latency).
Nmap scan report for paperclip.drexel.edu (144.118.67.71)
Host is up (0.015s latency).
Nmap scan report for imodules.irttest.drexel.edu (144.118.67.72)
Host is up (0.024s latency).
Nmap scan report for imodules.irt.drexel.edu (144.118.67.73)
Host is up (0.019s latency).
Nmap scan report for lists.irttest.drexel.edu (144.118.67.74)
Host is up (0.035s latency).
Nmap scan report for discover.drexel.edu (144.118.67.75)
Host is up (0.015s latency).
Nmap scan report for apps.research.drexel.edu (144.118.67.76)
Host is up (0.019s latency).
Nmap scan report for dragonfly.irttest.drexel.edu (144.118.67.77)
Host is up (0.017s latency).
Nmap scan report for nexus.irttest.drexel.edu (144.118.67.78)
Host is up (0.013s latency).
Nmap scan report for reccenter.irttest.drexel.edu (144.118.67.79)
Host is up (0.0075s latency).
Nmap scan report for events.drexel.edu (144.118.67.80)
Host is up (0.0099s latency).
Nmap scan report for 144.118.67.81
Host is up (0.0075s latency).
Nmap scan report for tower.irttest.drexel.edu (144.118.67.82)
Host is up (0.0078s latency).
Nmap scan report for bannerweb.drexel.edu (144.118.67.84)
Host is up (0.015s latency).
Nmap scan report for scgold-hosted.irttest.drexel.edu (144.118.67.85)
Host is up (0.0080s latency).
Nmap scan report for scgold-uat-edit.irt.drexel.edu (144.118.67.86)
Host is up (0.012s latency).
Nmap scan report for scgold-uat.irt.drexel.edu (144.118.67.87)
Host is up (0.013s latency).
Nmap scan report for 144.118.67.88
Host is up (0.011s latency).
Nmap scan report for accommodate.irttest.drexel.edu (144.118.67.89)
Host is up (0.0087s latency).
Nmap scan report for nexus.irt.drexel.edu (144.118.67.90)
Host is up (0.0087s latency).
Nmap scan report for mecmosd.irt.drexel.edu (144.118.67.92)
Host is up (0.016s latency).
Nmap scan report for scloudapi.irttest.drexel.edu (144.118.67.93)
Host is up (0.0063s latency).
Nmap scan report for scloudadm.irttest.drexel.edu (144.118.67.94)
Host is up (0.0091s latency).
Nmap scan report for policies.irttest.drexel.edu (144.118.67.95)
Host is up (0.0069s latency).
Nmap scan report for print.drexel.edu (144.118.67.97)
Host is up (0.010s latency).
Nmap scan report for 144.118.67.100
Host is up (0.020s latency).
Nmap scan report for 144.118.67.201
Host is up (0.010s latency).
Nmap scan report for biovx1.urcf.drexel.edu (144.118.78.21)
Host is up (0.013s latency).
Nmap scan report for biovx2.urcf.drexel.edu (144.118.78.22)
Host is up (0.0088s latency).
Nmap scan report for draco.physics.drexel.edu (144.118.78.25)
Host is up (0.016s latency).
Nmap scan report for sacan.urcf.drexel.edu (144.118.78.33)
Host is up (0.010s latency).
Nmap scan report for bioimagecx.urcf.drexel.edu (144.118.78.42)
Host is up (0.013s latency).
Nmap scan report for rloplotter.rlo.drexel.edu (144.118.95.170)
Host is up (0.011s latency).
Nmap scan report for r34keybox.rlo.drexel.edu (144.118.95.199)
Host is up (0.013s latency).
Nmap scan report for r26keybox.rlo.drexel.edu (144.118.95.207)
Host is up (0.0079s latency).
Nmap scan report for rracekeybox.rlo.drexel.edu (144.118.95.210)
Host is up (0.0079s latency).
Nmap scan report for r24keybox.rlo.drexel.edu (144.118.95.222)
Host is up (0.011s latency).
Nmap scan report for canon.excite.drexel.edu (144.118.173.220)
Host is up (0.0061s latency).
Nmap scan report for cicsp.cnhp.drexel.edu (144.118.185.144)
Host is up (0.0079s latency).
Nmap scan report for deansoperations.cnhp.drexel.edu (144.118.185.175)
Host is up (0.016s latency).
Nmap scan report for cnhplabresearch.cnhp.drexel.edu (144.118.185.176)
Host is up (0.012s latency).
Nmap scan report for ops01.cnhp.drexel.edu (144.118.185.178)
Host is up (0.022s latency).
Nmap scan report for vr.cnhp.drexel.edu (144.118.185.180)
Host is up (0.018s latency).
Nmap scan report for wkdu-stl-tx.student-org.drexel.edu (144.118.206.227)
Host is up (0.024s latency).
# Nmap done at Thu Feb 23 19:57:41 2023 -- 65536 IP addresses (151 hosts up) scanned in 2732.85 seconds
