# Network Security Analysis Project

##Project Topic:
Vulnerability Assessment of Drexel's Network. 

##Description:
There are many vulnerabilities that can be found on Drexel's network; but which ones pose the greatest risk? How you you help collect, synthezise and show the greatest risk?

##Mission
Our team plans to create a network vulnerability report of Drexel's network.  This will detail the vulnerabilities we find as well as the actions that we suggest Drexel's IT security team to take.  We will also be prioritizing these vulnerabilities, which will help the IT security team develop a possible plan of action for remediating these issues.



##Team Members:
Kieran Claus
Ben Frank
John Burress
Crystal Isaac
Max Simmons
Georgia Aidoo
