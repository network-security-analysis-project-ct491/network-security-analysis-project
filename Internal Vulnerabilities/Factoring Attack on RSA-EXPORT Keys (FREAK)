Factoring Attack on RSA-EXPORT Keys (FREAK)

What is it:
This vulnerability is related to weak website encryption. It can be exploited by an attacker via a man-in-the-middle attack by intercepting the HTTPS connection between the client and server.  
Systems with this vulnerability can potentially be forced to use export-grade cryptography, dating back to the 1990s-2000s.  By using export-grade cryptography, an attacker can decrypt the intercepted data very quickly.  
In order for a system to be vulnerable to FREAK, the server side must be compatible with export cipher suites, and the client must be using a vulnerable version of OpenSSL, Apple SecureTransport, Secure Channel, or offer an RSA export suite.  

Possible Solutions:
- Remove the support for export-grade cryptography on all systems in the environment.  
- Configure all edge devices to not allow clients to use export-grade cryptography.  This is a faster and still effective solution.  


Because this vulnerability was only found in the internal scan, it is highly possible that this vulnerability can not be expoited.  It seems the second solution from above might be in place; however, it still makes sense to disable legacy cryptography on all systems.


Priority: 2/10
